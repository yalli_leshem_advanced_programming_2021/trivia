#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <string>
#include "Game.h"
#include "Helper.h"
#include "Room.h"

class Game;
class Room;
class User
{
public:
	User(const std::string username, const SOCKET sock);

	void send(const std::string msg);

	//---------------------------------------------------Game related-------------------------------------------//
	/// <summary>
	/// set the game field to the param and reset the room field with nullptr
	/// </summary>
	/// <param name="gm">the game object to set</param>
	void setGame(Game* gm);

	/// <summary>
	/// if the user is in a game, leave that game
	/// </summary>
	/// <returns>true, if the game is still active, else, false</returns>
	bool leaveGame();

	//---------------------------------------------------Room related-------------------------------------------//
	/// <summary>
	/// if the user is already in a room, send an error message (114), if not create the room and send a message (114)
	/// </summary>
	/// <param name="roomId">the id of the room</param>
	/// <param name="roomName">the name of the room</param>
	/// <param name="maxUsers">maximum number of users that can join the room</param>
	/// <param name="questionsNo">number of questions</param>
	/// <param name="questionTime">amount of time to answer each question</param>
	/// <returns>true, if created a room successfully, else fale</returns>
	bool createRoom(const int roomId, const std::string roomName, const int maxUsers, const int questionsNo, const int questionTime);

	/// <summary>
	/// tries to join the room
	/// </summary>
	/// <param name="newRoom">the room the user wants to join</param>
	/// <returns>whether or not the user joined the room</returns>
	bool joinRoom(Room* newRoom);

	/// <summary>
	/// try to leave the room
	/// </summary>
	void leaveRoom();

	/// <summary>
	/// puts nullptr in the room field
	/// </summary>
	void clearRoom();

	/// <summary>
	/// tries to close the current room
	/// </summary>
	/// <returns>if succeeded, returns the room id, if not, returns -1</returns>
	int closeRoom();

	//---------------------------------------------------getters-------------------------------------------//
	std::string getUsername() const;
	SOCKET getSocket() const;
	Room* getRoom() const;
	Game* getGame() const;

private:
	std::string _username;
	SOCKET _sock;

	Room* _currRoom;
	Game* _currGame;
};

