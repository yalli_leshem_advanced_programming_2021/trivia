#include "Room.h"

Room::Room(const int id, User* admin, const std::string name, const int maxUsers, const int questionsNo, const int questionTime) 
	: _id(id), _admin(admin), _name(name), _maxUsers(maxUsers), _questionsNo(questionsNo), _questionTime(questionTime)
{
	this->_users.clear();
	this->_users.push_back(admin);
}

bool Room::joinRoom(User* user)
{
	std::string joinMsg = "110"; // success: [1100 questionsNumber questionTimeInSec] | failed: [1101]
	bool canJoinRoom = (this->_users.size() < this->_maxUsers); // if the room isn't full it is true

	if (canJoinRoom) 
	{
		this->_users.push_back(user);
		joinMsg.append("0");
		joinMsg.append(Helper::getPaddedNumber(this->_questionsNo, 2)); // number of questions
		joinMsg.append(Helper::getPaddedNumber(this->_questionTime, 2)); // time for each question
	}
	else
	{
		joinMsg.append("1");
	}

	user->send(joinMsg); // send msg

	if (canJoinRoom)
		this->sendMessage(this->getUsersListMessage());

	return canJoinRoom;
}

void Room::leaveRoom(User* user)
{
	this->_users.erase(std::remove(this->_users.begin(), this->_users.end(), user), this->_users.end()); // remove player from the list

	user->send("1120"); // response to leaving request
	this->sendMessage(this->getUsersListMessage());
}

int Room::closeRoom(const User* user)
{
	if (user != this->_admin) // if the user that requested to close the room isn't the admin
		return -1;

	this->sendMessage("116"); // send close message

	// use clearRoom for all users (but the admin)
	for (auto userC : this->_users)
	{
		if (userC != this->_admin) // don't use clearRoom on the admin
			userC->clearRoom();
	}

	return this->_id;
}

std::string Room::getUsersListMessage() const
{
	std::string msg = "108"; // msg format: [108 numberOfUsers ## username ## username]

	msg.append(std::to_string(this->_users.size())); // add number of users

	for (auto user : this->_users) // go through all users
	{
		msg.append(Helper::getPaddedNumber(user->getUsername().length(), 2)); // add length of username
		msg.append(user->getUsername()); // add username
	}

	return msg;
}

std::vector<User*> Room::getUsers() const { return this->_users; }
std::string Room::getName() const { return this->_name; }
int Room::getQuestionsNo() const { return this->_questionsNo; }
int Room::getId() const { return this->_id; }
User* Room::getAdmin() const { return this->_admin; }

void Room::sendMessage(const User* excludeUser, const std::string msg)
{
	// send msg to all users
	for (auto user : this->_users)
	{
		try // catch any errors and continue to the next user
		{
			if (user != excludeUser) // send the message to all users excluding the user param 
				user->send(msg);
		}
		catch (...) {}
	}
}

void Room::sendMessage(const std::string msg)
{
	this->sendMessage(nullptr, msg);
}

