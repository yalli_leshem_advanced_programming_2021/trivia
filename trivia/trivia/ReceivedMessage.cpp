#include "ReceivedMessage.h"

ReceivedMessage::ReceivedMessage(const SOCKET client_sock, const int msgCode) : _sock(client_sock), _messageCode(msgCode)
{
	this->_user = nullptr;
}

ReceivedMessage::ReceivedMessage(const SOCKET client_sock, const int msgCode, const std::vector<std::string> values) : _sock(client_sock), _messageCode(msgCode), _values(values)
{
	this->_user = nullptr;
}

SOCKET ReceivedMessage::getSock() const
{
	return this->_sock;
}

User* ReceivedMessage::getUser() const
{
	return this->_user;
}

int ReceivedMessage::getMessageCode() const
{
	return this->_messageCode;
}

std::vector<std::string> ReceivedMessage::getValues() const
{
	return this->_values;
}

void ReceivedMessage::setUser(User* user)
{
	this->_user = user;
}
