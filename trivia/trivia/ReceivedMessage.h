#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>
#include "User.h"

class ReceivedMessage
{
public:
	ReceivedMessage(const SOCKET client_sock, const int msgCode);
	ReceivedMessage(const SOCKET client_sock, const int msgCode, const std::vector<std::string> values);

	//---------------------------------------------------getters-------------------------------------------//
	SOCKET getSock() const;
	User* getUser() const;
	int getMessageCode() const;
	std::vector<std::string> getValues() const;

	void setUser(User* user);
private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	std::vector<std::string> _values;

};

