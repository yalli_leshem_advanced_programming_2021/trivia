#include "Game.h"

Game::Game(const std::vector<User*>& players, const int questionsNo, Database& db) : _db(db)
{
	// try to insert the game to the database
	if (!this->insertGameToDB()) // if false, didn't add to the db
	{
		std::cerr << "error while trying to add the game to db";
		throw std::exception("error while trying to add the game to db");
	}

	// initialize the rest of the fields
	this->_questionsNo = questionsNo;
	this->_currQuestionIndex = 0;

	this->initQuestionsFromDB();

	this->_players = players;
	this->initPlayersInf();
}

Game::~Game()
{
	// clear questions
	this->_questions.clear();

	this->_players.clear();
}

void Game::sendFirstQustion()
{
	this->sendQuestionToAllUsers();
}

void Game::handleFinishGame()
{
	// update game status in the database
	this->_db.updateGameStatus(this->_gameId);

	// build the finish game message [121 usersNumber ## userName score ##userName score...]
	std::string msg = "121";
	msg.append(std::to_string(int(this->_results.size()))); // number of users
	
	for (auto userRes : this->_results) // go through each result and add it to the message
	{
		msg.append(Helper::getPaddedNumber(int(userRes.first.size()), 2)); // username length
		msg.append(userRes.first); // username

		msg.append(Helper::getPaddedNumber(userRes.second, 2)); // score
	}

	// send message to all users
	std::cout << "trying to send msg: " << msg << std::endl;
	for (auto player : this->_players)
	{
		try // catch any errors and continue to the next user
		{
			player->send(msg);
			player->setGame(nullptr);
		}
		catch (...) {
			std::cerr << "err sending end game status" << std::endl;
		}
	}
}

bool Game::handleNextTurn()
{
	bool isGameActive = true;
	int numOfPlayers = int(this->_players.size()); // get number of active players

	if (numOfPlayers == 0) // if no more users are left in the game
	{
		this->handleFinishGame();
		isGameActive = false;
	}
	else if (numOfPlayers == this->_currentTurnAnswers) // check if all players answered the current question
	{
		if (this->_currQuestionIndex == this->_questionsNo - 1) // if already went through all the questions, finish the game
		{
			this->handleFinishGame();
			isGameActive = false;
		}
		else // if not, send the next question and inc the question index by 1
		{
			this->_currQuestionIndex++; // go to the next question
			this->sendQuestionToAllUsers(); // send the question to all users
		}
	}

	return isGameActive;
}

bool Game::handleAnswerFromUser(User* user, const int answerNo, const int time)
{
	std::string answer;
	std::string resMsg = "120"; // result message, [120 0/1]
	this->_currentTurnAnswers++;

	bool isCorrect = (answerNo -1 == this->_questions[this->_currQuestionIndex].getCorrectAnswerIndex());

	if (isCorrect) // if answered correctly
	{
		// add 1 point to the user
		std::map<std::string, int>::iterator it = this->_results.find(user->getUsername()); // find the username
		if (it != this->_results.end())
			this->_results[user->getUsername()]++;

		resMsg += "1"; // correct answer indication
	}
	else
	{
		resMsg += "0";
	}
	user->send(resMsg);

	if (answerNo < 5) // only 4 possible answers
	{
		answer = this->_questions[this->_currQuestionIndex].getAnswers()[answerNo - 1]; // get the answer
	}

	// add the players answer to the database
	this->_db.addAnswerToPlayer(this->_gameId, user->getUsername(), this->_questions[this->_currQuestionIndex].getId(), answer, isCorrect, time);

	return this->handleNextTurn();
}

bool Game::leaveGame(User* currUser)
{
	this->_players.erase(std::remove(this->_players.begin(), this->_players.end(), currUser), this->_players.end()); // remove player fro mthe list

	return this->handleNextTurn();
}

bool Game::insertGameToDB()
{
	this->_gameId = this->_db.insertNewGame(); // insert the game to the database

	return !(this->_gameId < 0); // if smaller then 0 returns false, else returns true
}

void Game::initQuestionsFromDB()
{
	this->_questions = this->_db.initQuestions(this->_questionsNo);
}

void Game::sendQuestionToAllUsers()
{
	this->_currentTurnAnswers = 0;
	std::string question = this->_questions[this->_currQuestionIndex].getQuestion();
	std::vector<std::string> answers = this->_questions[this->_currQuestionIndex].getAnswers();

	// build the message
	std::string msg = "118";
	msg.append(Helper::getPaddedNumber(int(question.length()), 3));
	msg.append(question);

	for (std::string answer : answers)
	{
		msg.append(Helper::getPaddedNumber(int(answer.length()), 3));
		msg.append(answer);
	}
	
	// send msg to all users
	for (auto player : this->_players)
	{
		try // catch any errors and continue to the next user
		{
			player->send(msg);
		}
		catch (...) {}
	}
}

void Game::initPlayersInf()
{
	for (auto player : this->_players) // go through all players
	{
		// add player to results map
		std::pair<std::string, int> playerRes;
		playerRes.first = player->getUsername();
		playerRes.second = 0;

		this->_results.insert(playerRes);
		// set the game as the users game
		player->setGame(this);
	}
}
