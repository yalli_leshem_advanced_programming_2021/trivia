#pragma once
#include <exception>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Question.h"
#include "sqlite3.h"

class Database
{
public:
	/// <summary>
	/// calls open
	/// </summary>
	Database();

	/// <summary>
	/// calls close
	/// </summary>
	~Database();

	//---------------------------------------------------User related-------------------------------------------//
	/// <summary>
	/// check if the user exists in the database
	/// </summary>
	/// <param name="username">the username to search for</param>
	/// <returns>Whether or not the user was found</returns>
	bool doesUserExist(const std::string username);

	/// <summary>
	/// check if the username and password match one of the users in the database
	/// </summary>
	/// <param name="username">the username to search for</param>
	/// <param name="password">the password to search for</param>
	/// <returns>whether or not the username and password match</returns>
	bool doesPasswordMatch(const std::string username, const std::string password);

	/// <summary>
	/// add new user to the users database
	/// </summary>
	/// <param name="username">the name of the new user</param>
	/// <param name="password">the password of the new user</param>
	/// <param name="email">the email of the new email</param>
	/// <returns>whether or not the user was added</returns>
	bool addNewUser(const std::string username, const std::string password, const std::string email);

	/// <summary>
	/// adds a new answer listing to the database
	/// </summary>
	/// <param name="gameId">the game id</param>
	/// <param name="username">the username of the user</param>
	/// <param name="questionId">the question id</param>
	/// <param name="answer">the answer</param>
	/// <param name="isCorrect">is the answer correct or not</param>
	/// <param name="answerTime">how many sec took the user to answer</param>
	/// <returns>did successfully add the listing or not</returns>
	bool addAnswerToPlayer(const int gameId, const std::string username, const int questionId, const std::string answer, const bool isCorrect, const int answerTime);

	//---------------------------------------------------Statistics and others-------------------------------------------//
	/// <summary>
	/// select randomly questionsNo amount of questions from the questions table
	/// </summary>
	/// <param name="questionsNo">the amount of questions</param>
	/// <returns>the questions</returns>
	std::vector<Question> initQuestions(const int questionsNo);

	/// <summary>
	/// gets the top 3 players with the highest scores
	/// </summary>
	/// <returns>the 3 highest scores</returns>
	std::map<std::string, int> getBestScores();

	/// <summary>
	/// gets the personal status of the user
	/// </summary>
	/// <param name="username">the username</param>
	/// <returns>personal status</returns>
	std::vector<std::string> getPersonalStatus(const std::string username);

	//---------------------------------------------------Game related-------------------------------------------//
	/// <summary>
	/// create a new game
	/// </summary>
	/// <returns>the new game's id</returns>
	int insertNewGame();

	/// <summary>
	/// updates the game status to 1 
	/// and the end time to NOW
	/// </summary>
	/// <param name="gameId">the game we want to update</param>
	/// <returns>whether or not the game stats was updated successfully</returns>
	bool updateGameStatus(const int gameId);

private:
	sqlite3* _DB;

	/// <summary>
	/// opens the database, returns false if there was an error
	/// </summary>
	/// <returns>true if nothing went wrong, else false</returns>
	bool open();

	/// <summary>
	/// closes the database
	/// </summary>
	void close();

	/// <summary>
	/// callback function reads the questions received by the database and formats them into a vector of questions 
	/// </summary>
	/// <param name="data">will be a list of questions</param>
	/// <param name="argc">The number of columns in row</param>
	/// <param name="argv">An array of strings representing fields in the row</param>
	/// <param name="azColName">An array of strings representing column names</param>
	/// <returns></returns>
	static int questionsTableCallback(void* data, int argc, char** argv, char** azColName);

	/// <summary>
	/// callback function reads the top 3 bestScores received by the database and formats them into a map
	/// </summary>
	/// <param name="data">will be a map of best scores</param>
	/// <param name="argc">The number of columns in row</param>
	/// <param name="argv">An array of strings representing fields in the row</param>
	/// <param name="azColName">An array of strings representing column names</param>
	/// <returns></returns>
	static int bestScoresTableCallback(void* data, int argc, char** argv, char** azColName);

	/// <summary>
	/// callback function reads the personal status received by the database and formats them into a vector of strings
	/// </summary>
	/// <param name="data">will be a vector of data</param>
	/// <param name="argc">The number of columns in row</param>
	/// <param name="argv">An array of strings representing fields in the row</param>
	/// <param name="azColName">An array of strings representing column names</param>
	/// <returns></returns>
	static int personalStatusTableCallback(void* data, int argc, char** argv, char** azColName);
};


