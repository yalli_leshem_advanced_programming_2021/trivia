#pragma once
#include <string>
#include <vector>
#include <map>
#include "User.h"
#include "Database.h"
#include "Question.h"

class User;

class Game
{
public:
	/// <summary>
	/// constructor, builds databbase object, initialize questions, players list and results
	/// </summary>
	/// <param name="players">the players in the game</param>
	/// <param name="questionsNo">number of questions in the game</param>
	/// <param name="db">the database</param>
	Game(const std::vector<User*>& players, const int questionsNo, Database& db);

	~Game();

	/// <summary>
	/// calls sendQuestionToAllUsers
	/// </summary>
	void sendFirstQustion();

	/// <summary>
	/// updates the game status in the database, sends end game msg (121) to all the players and resets there game field
	/// </summary>
	void handleFinishGame();

	/// <summary>
	/// make sure that there are still players in the game, if not, close it. check if all users answered the last question, if so, send the next question (if oit was the last question, end the game)  
	/// </summary>
	/// <returns>true, if the game is still active, else false</returns>
	bool handleNextTurn();

	/// <summary>
	/// checks if the user answered correctly, adds the answer to the database, send the user a message about his answer (120)
	/// </summary>
	/// <param name="user">user</param>
	/// <param name="answerNo"the index of the answer the user chose></param>
	/// <param name="time">the time that took for the user to answer</param>
	/// <returns>true, if the game is still active, else false</returns>
	bool handleAnswerFromUser(User* user, const int answerNo, const int time);

	/// <summary>
	/// tries to remove the user from the users list and calls "handleNextTurn"
	/// </summary>
	/// <param name="currUser">the user that asks to leave</param>
	/// <returns>true, if the game is still active, else false</returns>
	bool leaveGame(User* currUser);

private:
	int _gameId;
	std::vector<Question> _questions;
	std::vector<User*> _players;
	int _questionsNo;
	int _currQuestionIndex;
	Database& _db;
	std::map<std::string, int> _results;
	int _currentTurnAnswers;

	/// <summary>
	/// tries to add the game to the database
	/// </summary>
	/// <returns>true, if the game was added successfully, else, false</returns>
	bool insertGameToDB();

	/// <summary>
	/// initializes the questions from the database
	/// </summary>
	void initQuestionsFromDB();

	/// <summary>
	/// builds a "send question message (118)" and sends it to all users
	/// </summary>
	void sendQuestionToAllUsers();

	/// <summary>
	/// resets the results map, define for each player that this is it's current game 
	/// </summary>
	void initPlayersInf();
};

