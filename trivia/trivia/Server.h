#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <map>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <exception>
#include <deque>
#include "Helper.h"
#include "Validator.h"
#include "User.h"
#include "Room.h"
#include "ReceivedMessage.h"
#include "Database.h"

enum class ClientCodes
{
	SigninCode = 200,            // [200##username##pass]
	SignoutCode = 201,           // [201]
	SignupCode = 203,            // [203##username##pass##Email]
	GetRoomsCode = 205,          // [205]
	GetUsersInRoomCode = 207,    // [207roomID]
	JoinRoomCode = 209,          // [209roomID]
	LeaveRoomCode = 211,         // [211]
	CreateRoomCode = 213,        // [213##roomName playersNumberquestionsNumber questionTimeInSec]
	CloseRoomCode = 215,         // [215]
	StartGameCode = 217,		 // [217]
	SentAnswerCode = 219,		 // [219 answerNumber TimeInSeconds]
	LeaveGameCode = 222,		 // [222]
	GetBestScoresCode = 223,	 // [223]
	GetPersonalStatusCode = 225, // [225]
	ExitCode = 299		         // [299]
};

class Server
{
public:
	/// <summary>
	/// constructor, builds a new Database object and creates a new socket
	/// </summary>
	Server();

	/// <summary>
	/// D'tor, removes from memory both the users and rooms lists and closes the socket
	/// </summary>
	~Server();

	/// <summary>
	/// starts listening to clients and handles them
	/// </summary>
	void serve();

private:
	SOCKET _serverSocket;

	std::map< SOCKET, User* > _connectedUsers;
	Database _db;
	std::map<int, Room*> _roomList;

	std::mutex _mtxReceivedMessages;
	std::condition_variable _newMsgAlert;
	std::deque<ReceivedMessage> _queRcvMessages;

	static int _roomIdSequence;

	//---------------------------------------------------socket related-------------------------------------------//
	/// <summary>
	/// binds to socket 8889 and starts to listen, throws exception if either failed 
	/// </summary>
	void bindAndListen();

	// accept new client connections
	void accept();

	/// <summary>
	/// handles messages sent from the client
	/// </summary>
	/// <param name="client_soc">the socket of the client who's connected</param>
	void clientHandler(SOCKET client_soc);

	//---------------------------------------------------getters--------------------------------------------------//
	/// <summary>
	/// find the room from the rooms map
	/// </summary>
	/// <param name="roomId">the key to search for</param>
	/// <returns>the room requested (if found)</returns>
	Room* getRoomById(const int roomId) const;

	User* getUserByName(const std::string username) const;
	User* getUserBySocket(SOCKET client_soc) const;

	//---------------------------------------------------message related-------------------------------------------//
	/// <summary>
	/// builds a message from the client socket and calls addReceivedMessage
	/// </summary>
	/// <param name="client_soc">the socket of the client</param>
	/// <param name="msgCode">the code of the msg</param>
	void buildRecieveMessage(const SOCKET client_soc, const int msgCode);

	/// <summary>
	/// adds the message to the queue and notify handleReceivedMessages that a new message is waiting in queue
	/// </summary>
	/// <param name="msg">the message that needs to be pushed</param>
	void addReceivedMessage(const ReceivedMessage msg);

	/// <summary>
	/// handles a message in the queue (according to the msg code)
	/// </summary>
	void handleReceivedMessages();

	//---------------------------------------------------user related---------------------------------------------//
	/// <summary>
	/// safely remove user 
	/// </summary>
	/// <param name="msg">msg to remove the user</param>
	void safeDeleteUser(const ReceivedMessage& msg);

	/// <summary>
	/// delete the user from the users list, then calls: handleCloseRoom, handleLeaveRoom, handleLEaveGame 
	/// </summary>
	/// <param name="msg">the msg of the user</param>
	void handleSignout(const ReceivedMessage& msg);

	/// <summary>
	/// check if username and password match, if not or if the user is already singed in, send an error message to client accordingly. if all is well, add the user to the users list
	/// </summary>
	/// <param name="msg">the signin message</param>
	/// <returns>the new user (or nullptr if user wasn't created)</returns>
	User* handleSignin(const ReceivedMessage& msg);

	/// <summary>
	/// check if all the data is valid,
	///  if not send an error message accordingly,
	///  if it does, check if the user exists,
	///  if it does send an error message
	///  if all is well, add user to the database
	/// </summary>
	/// <param name="msg">the message received from the client</param>
	/// <returns>whether or not the user signed up</returns>
	bool handleSignup(const ReceivedMessage& msg);

	//---------------------------------------------------game related-------------------------------------------//
	/// <summary>
	/// leaves the game 
	/// </summary>
	/// <param name="msg">message received from the client</param>
	void handleLeaveGame(const ReceivedMessage& msg);

	/// <summary>
	/// starts the game
	/// </summary>
	/// <param name="msg">message received from the client</param>
	void handleStartGame(const ReceivedMessage& msg);

	/// <summary>
	/// handles the answer received from the player
	/// </summary>
	/// <param name="msg">message received from the client</param>
	void handlePlayerAnswer(const ReceivedMessage& msg);

	//---------------------------------------------------room related-------------------------------------------//
	/// <summary>
	/// create a new room with the values in the message object
	/// </summary>
	/// <param name="msg">message received from the client</param>
	/// <returns>if the room was created or not</returns>
	bool handleCreateRoom(const ReceivedMessage& msg);

	/// <summary>
	/// closes the room
	/// </summary>
	/// <param name="msg">the message received from the client</param>
	/// <returns>whether or not the room was closed</returns>
	bool handleCloseRoom(const ReceivedMessage& msg);

	/// <summary>
	/// tries to join the user to the room, if room isn't found, send error message
	/// </summary>
	/// <param name="msg">the message sent by the client</param>
	/// <returns>whether or not the user joined the room</returns>
	bool handleJoinRoom(const ReceivedMessage& msg);

	/// <summary>
	/// handles leave room request
	/// </summary>
	/// <param name="msg">the message sent by the client</param>
	/// <returns>whether or not the user left the room</returns>
	bool handleLeaveRoom(const ReceivedMessage& msg);
	
	/// <summary>
	/// handles a get users in room request
	/// </summary>
	/// <param name="msg">the message sent by the client</param>
	void handleGetUsersInRoom(const ReceivedMessage& msg);

	/// <summary>
	/// handles a get rooms request
	/// </summary>
	/// <param name="msg">the message sent by the client</param>
	void handleGetRooms(const ReceivedMessage& msg);

	//---------------------------------------------------statistics related-------------------------------------------//
	/// <summary>
	/// handles a get best scores request
	/// </summary>
	/// <param name="msg">the message sent by the client</param>
	void handleGetBestScores(const ReceivedMessage& msg);

	/// <summary>
	/// handles a get personal status request
	/// </summary>
	/// <param name="msg">the message sent by the client</param>
	void handleGetPersonalStatus(const ReceivedMessage& msg);
};

