#include "Database.h"

Database::Database()
{
	
	if (!this->open()) // if false then didn't open the database succefully
	{
		throw std::exception("Error while opening database");
	}
	std::cout << "created db" << std::endl;
}

Database::~Database()
{
	this->close();
}

bool Database::doesUserExist(const std::string username)
{
	std::string countQuery = "SELECT COUNT(*) FROM t_users WHERE username = '" + username + "';";

	sqlite3_stmt* stmt;
	int res = sqlite3_prepare_v2(this->_DB, countQuery.c_str(), -1, &stmt, NULL);
	if (res)
	{
		std::cerr << "Error At doesUserExist: couldn't find any existing User res: " << res << std::endl;
		std::cerr << "tried to prepare: " << countQuery << std::endl;

		return false;
	}
	else // check if count isn't 0
	{
		// send statement and receive count 
		sqlite3_step(stmt);
		int count = sqlite3_column_int(stmt, 0);
		sqlite3_finalize(stmt);
		return count > 0;
	}
}

bool Database::doesPasswordMatch(const std::string username, const std::string password)
{
	std::string countQuery = "SELECT COUNT(*) FROM t_users WHERE username = '" + username + "' AND password = '" + password + "'; ";

	sqlite3_stmt* stmt;
	int res = sqlite3_prepare_v2(this->_DB, countQuery.c_str(), -1, &stmt, NULL);
	if (res)
	{
		std::cerr << "Error At doesPasswordMatch: couldn't find any existing User res: " << res << std::endl;
		return false;
	}
	else // check if count isn't 0
	{
		// send statement and receive count 
		sqlite3_step(stmt);
		int count = sqlite3_column_int(stmt, 0);
		sqlite3_finalize(stmt);
		return count > 0;
	}
}

bool Database::addNewUser(const std::string username, const std::string password, const std::string email)
{
	std::string newUserQuery = "INSERT INTO t_users (username, password, email) VALUES(?, ?, ?);";
	sqlite3_stmt* stmt;

	// prepare statement
	int res = sqlite3_prepare_v2(this->_DB, newUserQuery.c_str(), -1, &stmt, NULL);
	if (res) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't prepare the INSERT user statement res: " << res << std::endl;
		return false;
	}
	else // add user to the database
	{
		// bind values to the statement
		sqlite3_bind_text(stmt, 1, username.c_str(), -1, NULL); // bind the name
		sqlite3_bind_text(stmt, 2, password.c_str(), -1, NULL); // bind the password
		sqlite3_bind_text(stmt, 3, email.c_str(), -1, NULL); // bind the email

		// execute the statement
		int code = sqlite3_step(stmt);
		sqlite3_finalize(stmt);

		if (code != SQLITE_DONE) // if any error happened
			return false;
	}
	return true;
}

bool Database::addAnswerToPlayer(const int gameId, const std::string username, const int questionId, const std::string answer, const bool isCorrect, const int answerTime)
{
	bool addedAnswer = true;
	int correct = (isCorrect) ? 1 : 0;
	std::string newAnswerQuery = "INSERT INTO t_players_answers(game_id, username, question_id, player_answer, is_correct, answer_time) VALUES(?, ?, ?, ?, ?, ?);";
	
	sqlite3_stmt* stmt;
	// prepare statement
	if (sqlite3_prepare_v2(this->_DB, newAnswerQuery.c_str(), -1, &stmt, NULL)) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't prepare the INSERT answer statement" << std::endl;
		return false;
	}
	else // add answer to the database
	{
		// bind values to the statement
		sqlite3_bind_int(stmt, 1, gameId); // bind the game id
		sqlite3_bind_text(stmt, 2, username.c_str(), -1, NULL); // bind the name
		sqlite3_bind_int(stmt, 3, questionId); // bind the question id
		sqlite3_bind_text(stmt, 4, answer.c_str(), -1, NULL); // bind the answer
		sqlite3_bind_int(stmt, 5, correct); // bind the result
		sqlite3_bind_int(stmt, 6, answerTime); // bind the time

		// execute the statement
		if (sqlite3_step(stmt) != SQLITE_DONE) // if any error accrued
			addedAnswer = false;

		sqlite3_finalize(stmt);
	}
	return addedAnswer;
}

std::vector<Question> Database::initQuestions(const int questionsNo)
{
	std::vector<Question> allQuestions;
	std::string getQuestionsQuery = "SELECT * FROM t_questions ORDER BY RANDOM() LIMIT " + std::to_string(questionsNo) + ";";

	// get the questions from the database
	if (sqlite3_exec(this->_DB, getQuestionsQuery.c_str(), Database::questionsTableCallback, &allQuestions, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error At getUser: couldn't get questions" << std::endl;
		throw std::exception(__FUNCTION__);
	}
	else
	{
		if (allQuestions.size() != questionsNo) // if didn't get the amount of questions requested
			throw std::exception(__FUNCTION__ " - amount of questions received aren't the same as the amount requested");
		else // if not return the questions
			return allQuestions;
	}
}

std::map<std::string, int> Database::getBestScores()
{
	std::map<std::string, int> bestScores;
	std::string getBestScoresQuery = "SELECT username, SUM(is_correct) as total FROM t_players_answers GROUP BY username ORDER BY total DESC LIMIT 3";

	// get the 3 best scores from the database
	if (sqlite3_exec(this->_DB, getBestScoresQuery.c_str(), Database::bestScoresTableCallback, &bestScores, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		std::cerr << "Error At getBestScores: couldn't get the scores" << std::endl;
		throw std::exception(__FUNCTION__);
	}
	
	return bestScores;
}

std::vector<std::string> Database::getPersonalStatus(const std::string username)
{
	std::vector<std::string> personalStatus;

	std::string numOfGamesQuery = "SELECT COUNT(DISTINCT game_id) FROM t_players_answers WHERE username = '" + username + "';";
	std::string numOfCorrectQuery = "SELECT COUNT(is_correct) FROM t_players_answers WHERE username = '" + username + "' AND is_correct = 1;";
	std::string numOfWrongQuery = "SELECT COUNT(is_correct) FROM t_players_answers WHERE username = '" + username + "' AND is_correct = 0;";
	std::string avgAnswerTimeQuery = "SELECT AVG(answer_time) FROM t_players_answers WHERE username = '" + username + "';";

	// get the 3 best scores from the database
	if (sqlite3_exec(this->_DB, numOfGamesQuery.c_str(), Database::personalStatusTableCallback, &personalStatus, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		//std::cerr << "Error At getPersonalStatus: couldn't get the number of games" << std::endl;
		personalStatus.clear();
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		return personalStatus;
	}
	if (sqlite3_exec(this->_DB, numOfCorrectQuery.c_str(), Database::personalStatusTableCallback, &personalStatus, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		//std::cerr << "Error At getPersonalStatus: couldn't get the numOfCorrect" << std::endl;
		personalStatus.clear();
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		return personalStatus;
	}
	if (sqlite3_exec(this->_DB, numOfWrongQuery.c_str(), Database::personalStatusTableCallback, &personalStatus, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		//std::cerr << "Error At getPersonalStatus: couldn't get the numOfWrong" << std::endl;
		personalStatus.clear();
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		return personalStatus;
	}
	if (sqlite3_exec(this->_DB, avgAnswerTimeQuery.c_str(), Database::personalStatusTableCallback, &personalStatus, NULL) != SQLITE_OK) // check that nothing went wrong
	{
		//std::cerr << "Error At getPersonalStatus: couldn't get the " << std::endl;
		personalStatus.clear();
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		personalStatus.push_back("0");
		return personalStatus;
	}

	return personalStatus;
}

int Database::insertNewGame()
{
	std::string newGameQuery = "INSERT INTO t_games values(null, 0, (DateTime('now')) , null);";
	std::string getIdQuery = "SELECT game_id FROM t_games WHERE rowid = (SELECT MAX(rowid) FROM t_games);";

	sqlite3_stmt* stmt;

	// prepare statement
	if (sqlite3_prepare_v2(this->_DB, newGameQuery.c_str(), -1, &stmt, NULL)) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't prepare the INSERT game statement" << std::endl;
		return -1;
	}
	else // add game to the database
	{
		// execute the statement
		int code = sqlite3_step(stmt);
		sqlite3_finalize(stmt);

		if (code != SQLITE_DONE) // if any error happened
			return -1;
	}
	
	// get the new game id
	sqlite3_stmt* stmt2;
	if (sqlite3_prepare_v2(this->_DB, getIdQuery.c_str(), -1, &stmt2, NULL))
	{
		std::cerr << "Error At insertNewGame" << std::endl;
		return -1;
	}
	else 
	{
		// send statement and receive count 
		sqlite3_step(stmt2);
		int game_id = sqlite3_column_int(stmt2, 0);
		sqlite3_finalize(stmt2);
		return game_id;
	}
}

bool Database::updateGameStatus(const int gameId)
{
	std::string updateGameStatusQuery = "UPDATE t_games SET status = 1, end_time = ( SELECT DateTime('now') )  WHERE game_id = " + std::to_string(gameId) + ";";

	sqlite3_stmt* stmt;

	// prepare statement
	if (sqlite3_prepare_v2(this->_DB, updateGameStatusQuery.c_str(), -1, &stmt, NULL)) // check that nothing went wrong
	{
		std::cerr << "Error: couldn't prepare the update game statement" << std::endl;
		return false;
	}
	else // update game status
	{
		// execute the statement
		int code = sqlite3_step(stmt);
		sqlite3_finalize(stmt);

		if (code != SQLITE_DONE) // if any error happened
			return false;
	}
}

bool Database::open()
{
	std::string dbFileName = "C:\\superman\\trivia.db";

	int res = sqlite3_open(dbFileName.c_str(), &this->_DB); 
	if (res != SQLITE_OK) {
		this->_DB = nullptr;
		std::cerr << "Failed to open DB" << std::endl;
		return false;
	}

	/*-------------------------make sure the table exist (will create the rest later)-------------------------*/
	std::string createUsersTable = "CREATE TABLE IF NOT EXISTS t_users(username TEXT NOT NULL PRIMARY KEY, password TEXT NOT NULL, email TEXT NOT NULL);";
	std::string createGameTable = "CREATE TABLE IF NOT EXISTS t_games(game_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, status INTEGER NOT NULL, start_time DATETIME NOT NULL, end_time DATETIME);";
	std::string createQuestionsTable = "CREATE TABLE IF NOT EXISTS t_questions(question_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, question TEXT NOT NULL, correct_ans TEXT NOT NULL, ans2 TEXT NOT NULL, ans3 TEXT NOT NULL, ans4 TEXT NOT NULL);";
	std::string createAnswersTable = "CREATE TABLE IF NOT EXISTS t_players_answers(game_id INTEGER NOT NULL, "
																				  "username TEXT NOT NULL, "
																				  "question_id INTEGER NOT NULL, "
																				  "player_answer TEXT NOT NULL, "
																				  "is_correct INTEGER NOT NULL, "
																				  "answer_time INTEGER NOT NULL, "
																				  "FOREIGN KEY(game_id) REFERENCES t_games(game_id), "
																				  "FOREIGN KEY(username) REFERENCES t_users(username), "
																				  "FOREIGN KEY(question_id) REFERENCES t_questions(question_id), "
																				  "PRIMARY KEY(game_id, username, question_id)) WITHOUT ROWID;";

	// send all commands
	if (sqlite3_exec(this->_DB, createUsersTable.c_str(), nullptr, nullptr, 0) != SQLITE_OK)
		std::cerr << "Error: didn't create users table";
	if (sqlite3_exec(this->_DB, createGameTable.c_str(), nullptr, nullptr, 0) != SQLITE_OK)
		std::cerr << "Error: didn't create games table";
	if (sqlite3_exec(this->_DB, createQuestionsTable.c_str(), nullptr, nullptr, 0) != SQLITE_OK)
		std::cerr << "Error: didn't create questions table";
	if (sqlite3_exec(this->_DB, createAnswersTable.c_str(), nullptr, nullptr, 0) != SQLITE_OK)
		std::cerr << "Error: didn't create answers table";

	return true;
}

void Database::close()
{
	sqlite3_close(this->_DB);
	this->_DB = nullptr;
}

int Database::questionsTableCallback(void* data, int argc, char** argv, char** azColName)
{
	int id = -1;
	std::string question;
	std::string correctAns;
	std::string ans2;
	std::string ans3;
	std::string ans4;

	std::vector<Question>* allQuestions = (std::vector<Question>*)data;

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "question_id") // cast the id into int
			id = atoi(argv[i]);
		else if (std::string(azColName[i]) == "question") // cast the question into string
		 	question = argv[i];
		else if (std::string(azColName[i]) == "correct_ans") // cast the correct answer into string
		 	correctAns = argv[i];
		else if (std::string(azColName[i]) == "ans2") // cast answer 2 into string
		 	ans2 = argv[i];
		else if (std::string(azColName[i]) == "ans3") // cast answer 3 into string
			ans3 = argv[i];
		else if (std::string(azColName[i]) == "ans4") // cast answer 4 into string
			ans4 = argv[i];
	}

	if (id > 0) // check that the data was not empty
		allQuestions->push_back(Question(id, question, correctAns, ans2, ans3, ans4));

	return 0;
}

int Database::bestScoresTableCallback(void* data, int argc, char** argv, char** azColName)
{
	int score;
	std::string username;

	std::map<std::string, int>* bestScores = (std::map<std::string, int>*)data;

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "username") // cast the name into string
			username = argv[i];
		else if (std::string(azColName[i]) == "total") // cast the score into int
			score = atoi(argv[i]);
	}

	if (!username.empty()) // check that the data was not empty
	{
		std::pair<std::string, int> pScore(username, score);
		bestScores->insert(pScore);
	}

	return 0;
}

int Database::personalStatusTableCallback(void* data, int argc, char** argv, char** azColName)
{
	std::vector<std::string>* personalStatus = (std::vector<std::string>*)data;

	if (argv[0] == NULL)
		return -1;

	for (int i = 0; i < argc; i++)
	{
		personalStatus->push_back(argv[i]);
	}


	return 0;
}
