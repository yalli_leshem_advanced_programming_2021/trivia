#include "Validator.h"

bool Validator::isPasswordValid(const std::string password)
{
	if (password.length() >= 4) // min length
	{
		// count to see if there is at least 1 of each (capital letter, normal letter and number)
		int dig = 0, capital = 0, normal = 0;
		for (int i = 0; i < password.length(); i++)
		{
			if (std::isspace(password[i])) // can't have spaces
				return false;

			if (std::isdigit(password[i])) // found one digit
				dig++; 
			else if (std::islower(password[i])) // found one lowercase letter
				normal++; 
			else if (std::isupper(password[i])) // found one capital latter
				capital++; 
		}
		if (dig == 0 || capital == 0 || normal == 0) 
			return false;
		else 
			return true; 
	}
	return false;
}

bool Validator::isUsernameValid(const std::string username)
{
	if (username.length() == 0) // can't be empty
		return false; 
	if (!isalpha(username[0])) // if first letter doesn't start with an alphabetical letter
		return false; 
	
	if (username.find(" ") == std::string::npos) // if didn't find any spaces
		return true;
	else
		return false;
}