#include "Question.h"

Question::Question(const int id, const std::string question, const std::string correctAnswer, const std::string answer2, const std::string answer3, const std::string answer4) : _id(id),  _question(question), _correctAnswerIndex(0)
{
	std::srand(unsigned(std::time(0)));

	// set the values:
	this->_answers.push_back(correctAnswer);
	this->_answers.push_back(answer2);
	this->_answers.push_back(answer3);
	this->_answers.push_back(answer4);

	// using built-in random generator:
	std::random_shuffle(this->_answers.begin(), this->_answers.end());

	for (int i = 0; i < this->_answers.size(); i++) // get the correct index
	{
		if (this->_answers[i] == correctAnswer)
			this->_correctAnswerIndex = i;
	}
}

std::string Question::getQuestion() const { return this->_question; }
std::vector<std::string> Question::getAnswers() const { return this->_answers; }
int Question::getCorrectAnswerIndex() const { return this->_correctAnswerIndex; }
int Question::getId() const { return this->_id; }