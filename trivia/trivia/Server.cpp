#include "Server.h"

int Server::_roomIdSequence = 0;

Server::Server()
{
	// create a new socket
	this->_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (this->_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}

	// build database object
	this->_db;
}

Server::~Server()
{
	// delete from memory the users list
	for (auto it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++) 
	{
		delete it->second;
	}
	this->_connectedUsers.clear();

	// delete from memory the rooms list
	for (auto it = this->_roomList.begin(); it != this->_roomList.end(); it++)
	{
		delete it->second;
	}
	this->_roomList.clear();
}

void Server::serve()
{
	this->bindAndListen();

	// start thread to handle new messages
	std::thread(&Server::handleReceivedMessages, this).detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add them to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		try
		{
			this->accept();
		}
		catch (...) {}
	}
}

void Server::bindAndListen()
{
	const int PORT = 8820;

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(this->_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(this->_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;
}

void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
	{
		std::cerr << "client error";
		throw std::exception(__FUNCTION__);
	}

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// make a client thread
	std::thread(&Server::clientHandler, this, client_socket).detach();
}

void Server::clientHandler(SOCKET client_soc)
{
	int msgCode = 0;

	try
	{
		do
		{
			msgCode = Helper::getMessageTypeCode(client_soc); // get message code

			// if message code is not exit/0 build the message
			if (msgCode != 0 && msgCode != int(ClientCodes::ExitCode))
			{
				this->buildRecieveMessage(client_soc, msgCode);
			}
		} while (msgCode != 0 && msgCode != int(ClientCodes::ExitCode));

	}
	catch (...){	} // no need to do anything, the action taken when an error occurred and when the client exits the server are the same

	// send an exit code 
	this->addReceivedMessage(ReceivedMessage(client_soc, int(ClientCodes::ExitCode)));
}

Room* Server::getRoomById(const int roomId) const
{
	auto it = this->_roomList.find(roomId);
	if (it != this->_roomList.end()) // if found the room
		return it->second;
	else // if not found
		return nullptr;
}

User* Server::getUserByName(const std::string username) const
{
	for (auto it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++) // go through all users
	{
		if (it->second->getUsername() == username) // if found the user requested
			return it->second;
	}

	return nullptr; // if the loop finished and got to this point, the username wasn't found  
}

User* Server::getUserBySocket(SOCKET client_soc) const
{
	auto it = this->_connectedUsers.find(client_soc);
	if (it != this->_connectedUsers.end()) // if user the room
		return it->second;
	else // if not found
		return nullptr;
}

void Server::buildRecieveMessage(const SOCKET client_soc, const int msgCode)
{
	switch (msgCode) 
	{
		case int(ClientCodes::SigninCode): // [2bytes, username, 2bytes, password]
		{
			// get values and push them into a vector
			std::string username = Helper::getStringPartFromSocket(client_soc, Helper::getIntPartFromSocket(client_soc, 2));
			std::string password = Helper::getStringPartFromSocket(client_soc, Helper::getIntPartFromSocket(client_soc, 2));

			std::vector<std::string> values;
			values.push_back(username);
			values.push_back(password);

			this->addReceivedMessage(ReceivedMessage(client_soc, msgCode, values)); // add message to the queue
			break;
		}
		case int(ClientCodes::SignupCode) : // [2bytes, username, 2bytes, password, 2bytes, email]
		{
			// get values and push them into a vector
			std::string username = Helper::getStringPartFromSocket(client_soc, Helper::getIntPartFromSocket(client_soc, 2));
			std::string password = Helper::getStringPartFromSocket(client_soc, Helper::getIntPartFromSocket(client_soc, 2));
			std::string email = Helper::getStringPartFromSocket(client_soc, Helper::getIntPartFromSocket(client_soc, 2));

			std::vector<std::string> values;
			values.push_back(username);
			values.push_back(password);
			values.push_back(email);

			this->addReceivedMessage(ReceivedMessage(client_soc, msgCode, values)); // add message to the queue
			break;
		}
		case int(ClientCodes::GetUsersInRoomCode) : // [roomId{4bytes}]
		{
			// get values and push them into a vector
			std::string roomId = Helper::getStringPartFromSocket(client_soc, 4);

			std::vector<std::string> values;
			values.push_back(roomId);

			this->addReceivedMessage(ReceivedMessage(client_soc, msgCode, values)); // add message to the queue
			break;
		}
		case int(ClientCodes::JoinRoomCode) : // [roomId{4bytes}]
		{
			// get values and push them into a vector
			std::string roomId = Helper::getStringPartFromSocket(client_soc, 4);

			std::vector<std::string> values;
			values.push_back(roomId);

			this->addReceivedMessage(ReceivedMessage(client_soc, msgCode, values)); // add message to the queue
			break;
		}
		case int(ClientCodes::CreateRoomCode) : // [2bytes, name, maxUsers{1byte}, questionsNo{2bytes}, time{2bytes}]
		{
			// get values and push them into a vector
			std::string name = Helper::getStringPartFromSocket(client_soc, Helper::getIntPartFromSocket(client_soc, 2));
			std::string maxUsers = Helper::getStringPartFromSocket(client_soc, 1);
			std::string questionsNo = Helper::getStringPartFromSocket(client_soc, 2);
			std::string questionTime= Helper::getStringPartFromSocket(client_soc, 2);

			std::vector<std::string> values;
			values.push_back(name);
			values.push_back(maxUsers);
			values.push_back(questionsNo);
			values.push_back(questionTime);

			this->addReceivedMessage(ReceivedMessage(client_soc, msgCode, values)); // add message to the queue
			break;
		}
		case int(ClientCodes::SentAnswerCode) : // [answerNo{1byte}, time{2bytes}]
		{
			// get values and push them into a vector
			std::string ansNo = Helper::getStringPartFromSocket(client_soc, 1);
			std::string ansTime = Helper::getStringPartFromSocket(client_soc, 2);

			std::cout << "ans: " << ansNo << " time: " << ansTime << std::endl;

			std::vector<std::string> values;
			values.push_back(ansNo);
			values.push_back(ansTime);

			this->addReceivedMessage(ReceivedMessage(client_soc, msgCode, values)); // add message to the queue
			break;
		}
		default: // the rest of the codes don't have any added values to them
		{
			this->addReceivedMessage(ReceivedMessage(client_soc, msgCode)); // add message to the queue
			break;
		}
	}
}

void Server::addReceivedMessage(const ReceivedMessage msg)
{
	std::lock_guard <std::mutex> lock(this->_mtxReceivedMessages);
	this->_queRcvMessages.push_back(msg);

	// When using blocking mode to get a message from a message queue, by condition Alert the waiting thread when a new message arrives 
	this->_newMsgAlert.notify_one();
}

void Server::handleReceivedMessages()
{
	while (true) // run until server is closed
	{
		// wait for a new message to be pushed in the queue
		std::unique_lock<std::mutex> lock(this->_mtxReceivedMessages);
		this->_newMsgAlert.wait(lock);

		// take the first message in queue and unlock the mutex
		ReceivedMessage msg = this->_queRcvMessages.front();
		this->_queRcvMessages.pop_front();

		lock.unlock(); // unlock the mutex

		// handle received message
		int msgCode = msg.getMessageCode();

		if (msgCode != int(ClientCodes::SigninCode) && msgCode != int(ClientCodes::SignupCode)) // only if user is already connected
			msg.setUser(this->getUserBySocket(msg.getSock())); // update user in message

		try { // if any error happens, delete user
			switch (msgCode)
			{
				case int(ClientCodes::SigninCode) : {
					this->handleSignin(std::ref(msg));
					break;
				}
				case int(ClientCodes::SignoutCode) : {
					this->handleSignout(std::ref(msg));
					break;
				}
				case int(ClientCodes::SignupCode) : {
					this->handleSignup(std::ref(msg));
					break;
				}
				case int(ClientCodes::GetRoomsCode) : {
					this->handleGetRooms(std::ref(msg));
					break;
				}
				case int(ClientCodes::GetUsersInRoomCode) : {
					this->handleGetUsersInRoom(std::ref(msg));
					break;
				}
				case int(ClientCodes::JoinRoomCode) : {
					this->handleJoinRoom(std::ref(msg));
					break;
				}
				case int(ClientCodes::LeaveRoomCode) : {
					this->handleLeaveRoom(std::ref(msg));
					break;
				}
				case int(ClientCodes::CreateRoomCode) : {
					this->handleCreateRoom(std::ref(msg));
					break;
				}
				case int(ClientCodes::CloseRoomCode) : {
					this->handleCloseRoom(std::ref(msg));
					break;
				}
				case int(ClientCodes::StartGameCode) : {
					this->handleStartGame(std::ref(msg));
					break;
				}
				case int(ClientCodes::SentAnswerCode) : {
					this->handlePlayerAnswer(std::ref(msg));
					break;
				}
				case int(ClientCodes::LeaveGameCode) : {
					this->handleLeaveGame(std::ref(msg));
					break;
				}
				case int(ClientCodes::GetBestScoresCode) : {
					this->handleGetBestScores(std::ref(msg));
					break;
				}
				case int(ClientCodes::GetPersonalStatusCode) : {
					this->handleGetPersonalStatus(std::ref(msg));
					break;
				}
				case int(ClientCodes::ExitCode) : {
					this->safeDeleteUser(std::ref(msg));
					break;
				}
				default: {
					this->safeDeleteUser(std::ref(msg));
					break;
				}
			}
		}
		catch (...)
		{
			this->safeDeleteUser(std::ref(msg));
		}
	}
}

void Server::safeDeleteUser(const ReceivedMessage& msg)
{
	try {
		SOCKET client_soc = msg.getSock();
		ReceivedMessage safeDeleteUserMsg = ReceivedMessage(client_soc, int(ClientCodes::SignoutCode));
		safeDeleteUserMsg.setUser(this->getUserBySocket(msg.getSock()));

		this->handleSignout(safeDeleteUserMsg);

		closesocket(client_soc);
	}
	catch (...) {}
}

void Server::handleSignout(const ReceivedMessage& msg)
{
	if (msg.getUser() != nullptr) // if the messag belongs to a user
	{
		this->_connectedUsers.erase(msg.getSock());
	}	
	this->handleCloseRoom(msg);
	this->handleLeaveRoom(msg);
	this->handleLeaveGame(msg);
}

User* Server::handleSignin(const ReceivedMessage& msg)
{
	std::string signinMsg = "102"; // [1020] -- success | [1021] -- wrong details | [1022] -- already connected
	User* user = nullptr;
	auto values = msg.getValues();
	std::string username = values.front(); // first value pushed was the user name
	std::string password = values.back(); // last value pushed is the password

	if (!this->_db.doesPasswordMatch(username, password)) // check if the username and password don't match
		signinMsg.append("1");
	else if (this->getUserByName(username) != nullptr) // if a value is returned, the user is already connected
		signinMsg.append("2");
	else // if theres no problem
	{
		signinMsg.append("0");
		// add user to the users list
		user = new User(username, msg.getSock());
		this->_connectedUsers[user->getSocket()] = user;
	}

	Helper::sendData(msg.getSock(), signinMsg); // send data

	return user;
}

bool Server::handleSignup(const ReceivedMessage& msg)
{
	std::string signupAns = "104"; // [1040] -- success | [1041] -- pass illegal | [1042] -- username already exists | [1043] -- username is illegal | [1044] -- other
	
	// get the values from the message
	auto values = msg.getValues();

	std::string email = values.back(); // last value pushed is the email
	values.pop_back(); // remove the last value from the vector 

	std::string password = values.back(); // second value pushed is the password
	values.pop_back(); // remove the last value from the vector 

	std::string username = values.back(); // first value pushed is the username
	values.pop_back(); // remove the last value from the vector 

	bool didSignUp = false;
	
	if (!Validator::isPasswordValid(password)) // check if password isn't valid
		signupAns.append("1");
	else if (!Validator::isUsernameValid(username)) // check if username isn't valid
		signupAns.append("3");
	else if (this->_db.doesUserExist(username)) // check if user already in the database
		signupAns.append("2");
	else
	{
		if (this->_db.addNewUser(username, password, email)) // if true, user was added successfully
		{
			signupAns.append("0");
			didSignUp = true;
		}
		else
			signupAns.append("4");
	}
	
	Helper::sendData(msg.getSock(), signupAns);
	return didSignUp;
}

void Server::handleLeaveGame(const ReceivedMessage& msg)
{
	msg.getUser()->leaveGame();
}

void Server::handleStartGame(const ReceivedMessage & msg)
{
	try
	{
		Room* room = msg.getUser()->getRoom();

		// create new game object and remove the room from the rooms list
		Game* game = new Game(room->getUsers(), room->getQuestionsNo(), std::ref(this->_db));

		this->_roomList.erase(room->getId());

		game->sendFirstQustion();
	}
	catch (...)
	{
		Helper::sendData(msg.getUser()->getRoom()->getAdmin()->getSocket(), "118000000");
	}
}

void Server::handlePlayerAnswer(const ReceivedMessage& msg)
{
	if (msg.getUser()->getGame()) // if not null
	{
		// get the values from the message
		auto values = msg.getValues();

		int time = atoi(values.back().c_str()); // last value pushed is the time

		int ansNo = atoi(values.front().c_str()); // first value pushed is the ans number

		if (!this->getUserBySocket(msg.getSock())->getGame()->handleAnswerFromUser(msg.getUser(), ansNo, time)) // if false then game is finished
		{
			Game* game = msg.getUser()->getGame();
			delete game;
		}
	}
}

bool Server::handleCreateRoom(const ReceivedMessage& msg)
{
	bool roomCreated = false;
	User* user = msg.getUser();

	if (user != nullptr)
	{
		// get the values from the message
		auto values = msg.getValues();

		int time = atoi(values.back().c_str()); // last value pushed is the time
		values.pop_back(); // remove the last value from the vector 

		int questionsNo = atoi(values.back().c_str()); // second to last value pushed is the number of questions
		values.pop_back(); // remove the last value from the vector 

		int maxUsers = atoi(values.back().c_str()); // second value pushed is the max amount of users
		values.pop_back(); // remove the last value from the vector 

		std::string name = values.back(); // first value pushed is the name
		values.pop_back(); // remove the last value from the vector 

		_roomIdSequence++; // inc room id 

		roomCreated = user->createRoom(_roomIdSequence, name, maxUsers, questionsNo, time); // return if room was created or not

		if (roomCreated) // if true, add to the list of rooms
			this->_roomList[_roomIdSequence] = user->getRoom();
	}

	return roomCreated;
}

bool Server::handleCloseRoom(const ReceivedMessage& msg)
{
	bool roomClosed = false;

	User* user = msg.getUser();

	if (user->getRoom() != nullptr)
	{
		int roomId = user->closeRoom(); // returns the room id (or -1 if failed)

		if (roomId != -1)
		{
			this->_roomList.erase(roomId);
			roomClosed = true;
		}
	}

	return roomClosed;
}

bool Server::handleJoinRoom(const ReceivedMessage& msg)
{
	bool joinedRoom = false;
	User* user = msg.getUser();

	if (user != nullptr)
	{
		// get the room id from the message
		int roomId = atoi(msg.getValues().back().c_str()); // only value pushed is the room id

		Room* room = this->getRoomById(roomId);

		if (room == nullptr)
		{
			user->send("1102");
		}
		else
		{	
			joinedRoom = user->joinRoom(room);
		}
	}

	return joinedRoom;
}

bool Server::handleLeaveRoom(const ReceivedMessage& msg)
{
	bool leaveRoom = false;
	User* user = msg.getUser();

	if (user != nullptr)
	{
		if (user->getRoom()) // if not null
		{
			leaveRoom = true;
			user->leaveRoom();
		}
	}

	return leaveRoom;
}

void Server::handleGetUsersInRoom(const ReceivedMessage& msg)
{
	User* user = msg.getUser();

	if (user != nullptr)
	{
		// get the room id from the message
		int roomId = atoi(msg.getValues().back().c_str()); // only value pushed is the room id

		Room* room = this->getRoomById(roomId);

		if (room == nullptr)
		{
			user->send("1080");
		}
		else
		{
			user->send(room->getUsersListMessage()); // send msg
		}
	}
}

void Server::handleGetRooms(const ReceivedMessage& msg)
{
	// build the message according to the protocol
	std::string roomListMsg = "106"; // [106 numberOfRooms roomID ## roomName roomID ## roomName...]

	roomListMsg.append(Helper::getPaddedNumber(this->_roomList.size(), 4));

	for (auto& room : this->_roomList) // go through all rooms
	{
		roomListMsg.append(Helper::getPaddedNumber(room.first, 4)); // add room id
		roomListMsg.append(Helper::getPaddedNumber(room.second->getName().length(), 2)); // add the size of the name
		roomListMsg.append(room.second->getName()); // add the name
	}

	Helper::sendData(msg.getSock(), roomListMsg);
}

void Server::handleGetBestScores(const ReceivedMessage& msg)
{
	std::string bestScoresMsg = "124"; // [124 ## userName highestScore ## userName	highestScore ## userName highestScore]

	auto bestScores = this->_db.getBestScores();

	for (auto& score : bestScores) // go through all scores
	{
		bestScoresMsg.append(Helper::getPaddedNumber(score.first.length(), 2)); // add the size of the name
		bestScoresMsg.append(score.first); // add the size of the name
		bestScoresMsg.append(Helper::getPaddedNumber(score.second, 6)); // add the score
	}

	int emptySlots = 3 - bestScores.size(); // check if there aren't 3 players
	for (int i = 0; i < emptySlots; i++) // add the remaining players as 00
		bestScoresMsg.append("00000000");

	Helper::sendData(msg.getSock(), bestScoresMsg);
}

void Server::handleGetPersonalStatus(const ReceivedMessage& msg)
{
	// get the values from the database
	auto values = this->_db.getPersonalStatus(msg.getUser()->getUsername());

	int avgTime = atoi(values.back().c_str()); // last value pushed is the avg time
	values.pop_back(); // remove the last value from the vector 

	int wrongAnsNo = atoi(values.back().c_str()); // second to last value pushed is the number of wrong and
	values.pop_back(); // remove the last value from the vector 


	int correctAnsNo = atoi(values.back().c_str()); // second value pushed is the amount of correct ans
	values.pop_back(); // remove the last value from the vector

	int gamesNo = atoi(values.back().c_str()); // first value is the amount of games
	values.pop_back(); // remove the last value from the vector

	std::string personalStatusMsg = "126"; // [126 numberOfGames numberOfRightAns numerOfWrongAns avgTimeForAns]
	personalStatusMsg.append(Helper::getPaddedNumber(gamesNo, 4));
	personalStatusMsg.append(Helper::getPaddedNumber(correctAnsNo, 6));
	personalStatusMsg.append(Helper::getPaddedNumber(wrongAnsNo, 6));
	personalStatusMsg.append(Helper::getPaddedNumber(avgTime, 4));

	Helper::sendData(msg.getSock(), personalStatusMsg);
}
