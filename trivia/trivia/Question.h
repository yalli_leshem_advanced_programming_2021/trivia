#pragma once
#include <string>
#include <iostream>     // std::cout
#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand

class Question
{
public:
	Question(const int id, const std::string question, const std::string correctAnswer, const std::string answer2, const std::string answer3, const std::string answer4);

	//---------------------------------------------------getters-------------------------------------------//
	std::string getQuestion() const;
	std::vector<std::string> getAnswers() const;
	int getCorrectAnswerIndex() const;
	int getId() const;

private:
	std::string _question;
	std::vector<std::string> _answers;
	int _correctAnswerIndex;
	int _id;
};

