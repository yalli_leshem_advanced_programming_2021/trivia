#pragma once
#include <string>
#include <vector>
#include "User.h"

class User;
class Room
{
public:
	Room(const int id, User* admin, const std::string name, const int maxUsers, const int questionsNo, const int questionTime);

	/// <summary>
	/// tries to add user to the room
	/// </summary>
	/// <param name="user">the user that wants to join the room</param>
	/// <returns>true, if the user joined successfully, else false</returns>
	bool joinRoom(User* user);

	/// <summary>
	/// remove the user from the room
	/// </summary>
	/// <param name="user">the user that wants to leave</param>
	void leaveRoom(User* user);

	/// <summary>
	/// close the room (only if the admin is the one that requested so)
	/// </summary>
	/// <param name="user">the user that requested to close the room</param>
	/// <returns>-1, if the user isn't the admin, if the user is the admin, return the room id</returns>
	int closeRoom(const User* user);

	//---------------------------------------------------getters-------------------------------------------//
	/// <summary>
	/// builds a userList message
	/// </summary>
	/// <returns>returns a usersList message [108 numberOfUsers ## username ## username...]</returns>
	std::string getUsersListMessage() const;
	User* getAdmin() const;
	std::vector<User*> getUsers() const;
	std::string getName() const;
	int getQuestionsNo() const;
	int getId() const;

private:
	std::vector<User*> _users;
	User* _admin;

	int _maxUsers;
	int _questionTime;
	int _questionsNo;

	std::string _name;
	int _id;

	/// <summary>
	/// send all users (except for the excludeUser) the message 
	/// </summary>
	/// <param name="excludeUser">user that will not receive the message</param>
	/// <param name="msg">the message to send to all the users</param>
	void sendMessage(const User* excludeUser, const std::string msg);

	/// <summary>
	/// calls sendMessage with nullptr as the excludeUser
	/// </summary>
	/// <param name="msg">the message to send</param>
	void sendMessage(const std::string msg);
};

