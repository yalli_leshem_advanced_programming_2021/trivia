#pragma once
#include <string>

class Validator
{
public:
	static bool isPasswordValid(const std::string password);
	static bool isUsernameValid(const std::string username);
};

