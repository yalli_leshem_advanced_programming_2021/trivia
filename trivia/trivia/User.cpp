#include "User.h"

User::User(const std::string username, const SOCKET sock) : _username(username), _sock(sock)
{
	this->_currGame = nullptr;
	this->clearRoom();
}

void User::send(const std::string msg)
{
	Helper::sendData(this->_sock, msg); // send the message
}

void User::setGame(Game* gm)
{
	this->_currGame = gm;
	this->clearRoom();
}

bool User::leaveGame()
{
	bool stillActive = false;

	if (this->_currGame) // if there is a game object still active
	{
		stillActive = this->_currGame->leaveGame(this);
		this->_currGame = nullptr;
	}

	return stillActive;
}

bool User::createRoom(const int roomId, const std::string roomName, const int maxUsers, const int questionsNo, const int questionTime)
{
	bool createdRoom = false;
	std::string createRoomRes = "114"; // [1140] -- success | [1141] -- failed

	if (this->_currRoom != nullptr) // if already in a room
	{
		createRoomRes.append("1"); // failed
	}
	else
	{
		this->_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime); // create room object
		createRoomRes.append("0"); // succeeded
		createdRoom = true;
	}

	this->send(createRoomRes);
	return createdRoom;
}

bool User::joinRoom(Room* newRoom)
{
	bool joinedRoom = (this->_currRoom == nullptr && newRoom != nullptr); // if true then the user isn't in a room and can try to join the new one

	if (joinedRoom) // if true, currRoom is empty
	{
		this->_currRoom = newRoom;
		joinedRoom = newRoom->joinRoom(this); // tries to join the room
	}

	return joinedRoom;
}

void User::leaveRoom()
{
	if (this->_currRoom != nullptr) // if in a room
	{
		this->_currRoom->leaveRoom(this);
		this->clearRoom();
	}
}

void User::clearRoom()
{
	this->_currRoom = nullptr;
}

int User::closeRoom()
{
	int roomId = -1;

	if (this->_currRoom != nullptr) // if in a room
	{
		roomId = this->_currRoom->closeRoom(this); // try to close the room

		if (roomId != -1) // if closed the room
		{
			// delete the room pointer and point to nullptr
			delete this->_currRoom;
			this->clearRoom();
		}
	}

	return roomId;
}

std::string User::getUsername() const { return this->_username; }
SOCKET User::getSocket() const { return this->_sock; }
Room* User::getRoom() const { return this->_currRoom; }
Game* User::getGame() const { return this->_currGame; }