﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for Signup.xaml
    /// </summary>
    public partial class Signup : Window
    {
        public Signup()
        {
            InitializeComponent();
        }

        bool emailAlreadyReset = false;
        private void EmailInput_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!emailAlreadyReset) // only if it's the first click on the box
            {
                EmailInput.Text = ""; // Clears the text field
                emailAlreadyReset = true;
            }
        }

        bool userAlreadyReset = false;
        private void UsernameInput_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!userAlreadyReset) // only if it's the first click on the box
            {
                UsernameInput.Text = ""; // Clears the text field
                userAlreadyReset = true;
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            UsernameInput.Text = "Username";
            EmailInput.Text = "Email";
            VisiblePass.Text = "Password";
            userAlreadyReset = false;
            emailAlreadyReset = false;
        }

        /// <summary>
        /// when hovering over the checkbox, show the hidden textbox 
        /// </summary>
        private void ShowPass_MouseEnter_1(object sender, MouseEventArgs e)
        {
            ShowPassText.Visibility = Visibility.Visible;

            // hide the passwordbox and show the textbox
            Password.Visibility = Visibility.Hidden;
            VisiblePass.Visibility = Visibility.Visible;
        }

        private void Password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            VisiblePass.Text = Password.Password;
        }

        private void ShowPass_MouseLeave_1(object sender, MouseEventArgs e)
        {
            ShowPassText.Visibility = Visibility.Hidden;
            VisiblePass.Visibility = Visibility.Hidden;
            Password.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// when mouse enters the password field, hide the text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VisiblePass_MouseEnter(object sender, MouseEventArgs e)
        {
            VisiblePass.Visibility = Visibility.Hidden;
        }

        private void SignupButton_Click(object sender, RoutedEventArgs e)
        {
            // when clicked, send data to the server
            // TODO:
            // send data to server 
            // signup/throw error, depending on server response
            sendSignupMsg();

            char res = getSignupRsp();

            if (res == '0') // if 0 then succeeded
            {
                // when clicked, go to login window
                Owner.Show();
                this.Close();
            }
            else if (res == '1') // pass illegal 
            {
                errorPopUp.Content = "Pass illegal";
                UsernameInput.Text = "Username";
                EmailInput.Text = "Email";
                VisiblePass.Text = "Password";
                Password.Password = "";
                userAlreadyReset = false;
                emailAlreadyReset = false;
            }
            else if  (res == '2') // username taken
            {
                errorPopUp.Content = "Username taken";
                UsernameInput.Text = "Username";
                EmailInput.Text = "Email";
                VisiblePass.Text = "Password";
                Password.Password = "";
                userAlreadyReset = false;
                emailAlreadyReset = false;
            }
            else if (res == '3') // username illegL
            {
                errorPopUp.Content = "Username Illegal";
                UsernameInput.Text = "Username";
                EmailInput.Text = "Email";
                VisiblePass.Text = "Password";
                Password.Password = "";
                userAlreadyReset = false;
                emailAlreadyReset = false;
            }
            else
            {
                errorPopUp.Content = "Other";
                UsernameInput.Text = "Username";
                EmailInput.Text = "Email";
                VisiblePass.Text = "Password";
                Password.Password = "";
                userAlreadyReset = false;
                emailAlreadyReset = false;
            }
        }

        /// <summary>
        /// sends the signup message
        /// </summary>
        private void sendSignupMsg()
        {
            string userLen = Convert.ToString(UsernameInput.Text.Length);
            string signupMsg = "203" + userLen.PadLeft(2, '0') + UsernameInput.Text;

            string passLen = Convert.ToString(Password.Password.Length);
            signupMsg += passLen.PadLeft(2, '0') + Password.Password;

            string emailLen = Convert.ToString(EmailInput.Text.Length);
            signupMsg += emailLen.PadLeft(2, '0') + EmailInput.Text;

            MainWindow.TCPSendData(signupMsg);
        }

        /// <summary>
        /// returns code
        /// </summary>
        /// <returns></returns>
        private char getSignupRsp()
        {
            string response = MainWindow.TCPGetData();
            return response[3];
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            // when clicked, go to login window
            Owner.Show();
            this.Close();
        }
    }
}
