﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for BestScoresWindow.xaml
    /// </summary>
    public partial class BestScoresWindow : Window
    {

        private void getBestScores()
        {
            string rsp = MainWindow.TCPGetData();

            int currIndx = 3; // start from after the code (3 bytes)

            string userLen = rsp.Substring(currIndx, 2); // len is 2 bytes
            currIndx += 2;
            string user1 = rsp.Substring(currIndx, int.Parse(userLen));
            currIndx += int.Parse(userLen);

            int user1Score = int.Parse(rsp.Substring(currIndx, 6)); // 6 bytes is the score
            currIndx += 6;

            User1.Text = user1 + " - " + user1Score.ToString();

            string user2Len = rsp.Substring(currIndx, 2); // len is 2 bytes
            currIndx += 2;
            string user2 = rsp.Substring(currIndx, int.Parse(user2Len));
            currIndx += int.Parse(user2Len);

            int user2Score = int.Parse(rsp.Substring(currIndx, 6)); // 6 bytes is the score
            currIndx += 6;

            User2.Text = user2 + " - " + user2Score.ToString();

            string user3Len = rsp.Substring(currIndx, 2); // len is 2 bytes
            currIndx += 2;
            string user3 = rsp.Substring(currIndx, int.Parse(user3Len));
            currIndx += int.Parse(user3Len);

            int user3Score = int.Parse(rsp.Substring(currIndx, 6)); // 6 bytes is the score
            User3.Text = user3 + " - " + user3Score.ToString();
        }

        public BestScoresWindow()
        {
            InitializeComponent();
            MainWindow.TCPSendData("223");

            getBestScores();
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            // close window and open menu window
            this.Owner.Show();
            this.Close();
        }
    }
}
