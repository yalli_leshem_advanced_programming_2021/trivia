﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for CreateRoomWindow.xaml
    /// </summary>
    public partial class CreateRoomWindow : Window
    {
        public CreateRoomWindow(string userName)
        {
            InitializeComponent();
            ShowLoggedUser.Content = userName;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            // go back to the menu screen
            this.Owner.Show();
            this.Close();
        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            string nameLen = Convert.ToString(RoomNameBox.Text.Length);
            string createRoomMsg = "213" + nameLen.PadLeft(2, '0') + RoomNameBox.Text;

            createRoomMsg += PlayersNumSlider.Value.ToString();
            createRoomMsg += QuestionsNumSlider.Value.ToString().PadLeft(2, '0');
            createRoomMsg += TimeSlider.Value.ToString().PadLeft(2, '0');

            MainWindow.TCPSendData(createRoomMsg);

            if (MainWindow.TCPGetData()[3] == '0') // if success
            {
                string waitingRoomInf = "Room: " + RoomNameBox.Text + "\nRoom Capacity: " + PlayersNumSlider.Value.ToString() + "\nNumber Of Questions: " + QuestionsNumSlider.Value.ToString() + "\nTime Per Round:" + TimeSlider.Value.ToString();
                WaitingRoomWindow waitingWin = new(ShowLoggedUser.Content.ToString(), waitingRoomInf, TimeSlider.Value.ToString(), QuestionsNumSlider.Value.ToString(), true);
                waitingWin.Owner = this.Owner; // make the menu window it's owner
                waitingWin.Show();
                this.Close(); // close this window
            }
        }

        bool nameAlreadyReset = false;
        private void RoomNameBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!nameAlreadyReset) // only if it's the first click on the box
            {
                RoomNameBox.Text = ""; // Clears the text field
                nameAlreadyReset = true;
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            RoomNameBox.Text = "Room Name";
            PlayersNumSlider.Value = 1;
            QuestionsNumSlider.Value = 1;
            TimeSlider.Value = 1;
            nameAlreadyReset = false;
        }
    }
}
