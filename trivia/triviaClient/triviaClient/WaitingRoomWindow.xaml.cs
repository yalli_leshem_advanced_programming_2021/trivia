﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for WaitingRoomWindow.xaml
    /// </summary>
    public partial class WaitingRoomWindow : Window
    {

        private BackgroundWorker background_worker = new BackgroundWorker();
        private string NumOfQ;
        private string TimePerQ;
        public WaitingRoomWindow(string userName, string roomInf, string time, string NoQues, bool isOwner = false)
        {
            InitializeComponent();
            if (isOwner) // only if the user is the owner enable start and close room
            {
                StartGameButton.IsEnabled = true;
                CloseRoomButton.IsEnabled = true;
                ExitRoomButton.IsEnabled = false;
                ExitRoomButton.Visibility = Visibility.Hidden;
                StartGameButton.Visibility = Visibility.Visible;
                CloseRoomButton.Visibility = Visibility.Visible;
                UserList.Text = userName;
            }   
            else // if user joined
            {
                StartGameButton.IsEnabled = false;
                CloseRoomButton.IsEnabled = false;
                ExitRoomButton.IsEnabled = true;
                ExitRoomButton.Visibility = Visibility.Visible;
                StartGameButton.Visibility = Visibility.Hidden;
                CloseRoomButton.Visibility = Visibility.Hidden;
            }
            ShowLoggedUser.Content = userName;
           
            RoomInfo.Text = roomInf;
            NumOfQ = NoQues;
            TimePerQ = time;

            background_worker.WorkerSupportsCancellation = true;
            background_worker.WorkerReportsProgress = true;

            background_worker.DoWork += AutoUpdateUsers;
            background_worker.ProgressChanged += UpdateUsers;
            background_worker.RunWorkerAsync();
        }

        void AutoUpdateUsers(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (background_worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                string rsp = MainWindow.TCPGetData();
                background_worker.ReportProgress(0, rsp);
                Thread.Sleep(1000);
            }
        }  

        void UpdateUsers(object sender, ProgressChangedEventArgs e)
        {
            int currIndx = 0;

            string rsp = e.UserState as string;
            string msgCode = rsp.Substring(currIndx, 3); // len is 2 bytes
            currIndx += 3;

            if (msgCode == "116") // room closed
            {
                // close window, return to menu window, send close room message
                background_worker.CancelAsync();
                background_worker.Dispose();
                this.Owner.Show();
                this.Close();
            }
            else if (msgCode == "108") // user list
            {
                UserList.Text = "";
                int usersNo = int.Parse(rsp.Substring(currIndx, 1));
                currIndx += 1;

                for (int i = 0; i < usersNo; i++) // show all users
                {
                    int userLen = int.Parse(rsp.Substring(currIndx, 2)); // len is 2 bytes
                    currIndx += 2;
                    string user = rsp.Substring(currIndx, userLen);
                    currIndx += userLen;

                    UserList.Text += user + "\n";
                }
            }
            else if (msgCode == "118") // start game
            {
                GameWindow gameWin = new(ShowLoggedUser.Content.ToString(), NumOfQ, TimePerQ, rsp);
                gameWin.Owner = this.Owner;
                gameWin.Show();

                background_worker.CancelAsync();
                background_worker.Dispose();
                this.Close();
            }
            else if (msgCode == "112") // left room
            {
                // close window, return to menu window, send close room message
                background_worker.CancelAsync();
                background_worker.Dispose();
                this.Owner.Show();
                this.Close();
            }
        }

        private void StartGameButton_Click(object sender, RoutedEventArgs e)
        {
            // open trivia window
            MainWindow.TCPSendData("217");
        }

        private void CloseRoomButton_Click(object sender, RoutedEventArgs e)
        {
            // close window, return to menu window, send close room message
            MainWindow.TCPSendData("215");
        }

        private void ExitRoomButton_Click(object sender, RoutedEventArgs e)
        {
            // close window, return to menu window, send close room message
            MainWindow.TCPSendData("211");
        }
    }
}
