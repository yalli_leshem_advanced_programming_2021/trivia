﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Timers;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        private Timer timer;
        private string questionsNum;
        private int currQuestion = 0;
        private int correctAnswers = 0;
        public GameWindow(string userName, string questionsNo, string time, string rsp="noneYet")
        {
            InitializeComponent();

            // initialize fields
            QuestionTimer.Maximum = int.Parse(time);
            questionsNum = questionsNo;

            timer = new Timer(1000);
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);

            if (rsp == "noneYet")
                GetNextQuestion();
            else
                FormatNextQuestion(rsp);
        }

        private void GetNextQuestion()
        {
            FormatNextQuestion(MainWindow.TCPGetData());
        }
            
        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)(() =>
            {
                if (QuestionTimer.Value > 0)
                {
                    QuestionTimer.Value -= 1;
                }
                else
                {
                    SendAnswer("5"); // time ran out
                }
            }));
        }
        private void FormatNextQuestion(string rsp)
        {
            int currIndx = 0;

            string msgCode = rsp.Substring(currIndx, 3); // len is 2 bytes
            currIndx += 3;

            if (msgCode == "118") // sends next question code
            {
                int questionLen = int.Parse(rsp.Substring(currIndx, 3)); // len is 3 bytes
                currIndx += 3;

                if (questionLen == 0) // if 0 somthing happend
                {
                    MessageBox.Show("Something Went Wrong");
                    this.Owner.Show();
                    timer.Close();
                    this.Close();
                }
                string question = rsp.Substring(currIndx, questionLen);
                currIndx += questionLen;

                int ans1Len = int.Parse(rsp.Substring(currIndx, 3)); // len is 3 bytes
                currIndx += 3;
                string ans1 = rsp.Substring(currIndx, ans1Len);
                currIndx += ans1Len;

                int ans2Len = int.Parse(rsp.Substring(currIndx, 3)); // len is 3 bytes
                currIndx += 3;
                string ans2 = rsp.Substring(currIndx, ans2Len);
                currIndx += ans2Len;

                int ans3Len = int.Parse(rsp.Substring(currIndx, 3)); // len is 3 bytes
                currIndx += 3;
                string ans3 = rsp.Substring(currIndx, ans3Len);
                currIndx += ans3Len;

                int ans4Len = int.Parse(rsp.Substring(currIndx, 3)); // len is 3 bytes
                currIndx += 3;
                string ans4 = rsp.Substring(currIndx, ans4Len);
                currIndx += ans4Len;

                UpdateWindow(question, ans1, ans2, ans3, ans4);
                timer.Start();
            }
            else if (msgCode == "121") // end of game code
            {
                string scoreMsg = "";

                int usersNo = int.Parse(rsp.Substring(currIndx, 1));
                currIndx += 1;

                for (int i = 0; i < usersNo; i++) // show all users score
                {
                    int userLen = int.Parse(rsp.Substring(currIndx, 2)); // len is 2 bytes
                    currIndx += 2;
                    string user = rsp.Substring(currIndx, userLen);
                    currIndx += userLen;

                    string score = rsp.Substring(currIndx, 2); // score is 2 bytes
                    currIndx += 2;

                    scoreMsg += "User: " + user + "    Score: " + score + "\n";
                }

                timer.Close();
                MessageBox.Show(scoreMsg, "Scores");
                this.Owner.Show();
                this.Close();
            }
        }

        private async void SendAnswer(string ansNo)
        {
            // stop timer
            timer.Stop();

            string ansTime = (QuestionTimer.Maximum - QuestionTimer.Value).ToString();
            string ansMsg = "219" + ansNo + ansTime.PadLeft(2, '0');

            MainWindow.TCPSendData(ansMsg);

            var ogColor = Ans1.Background;
            if (MainWindow.TCPGetData()[3] == '1')
            {
                correctAnswers++;
                if (ansNo == "1")
                    Ans1.Background = Brushes.Green;
                else if (ansNo == "2")
                    Ans2.Background = Brushes.Green;
                else if (ansNo == "3")
                    Ans3.Background = Brushes.Green;
                else if (ansNo == "4")
                    Ans4.Background = Brushes.Green;
            }
            else // if wrong
            {
                if (ansNo == "1")
                    Ans1.Background = Brushes.Red;
                else if (ansNo == "2")
                    Ans2.Background = Brushes.Red;
                else if (ansNo == "3")
                    Ans3.Background = Brushes.Red;
                else if (ansNo == "4")
                    Ans4.Background = Brushes.Red;
            }

            // wait 0.5 sec
            await Task.Delay(500);
            Ans1.Background = ogColor;
            Ans2.Background = ogColor;
            Ans3.Background = ogColor;
            Ans4.Background = ogColor;

            GetNextQuestion();
        }           

        private void UpdateWindow(string question, string ans1, string ans2, string ans3, string ans4)
        {
            QuestionTimer.Value = QuestionTimer.Maximum;

            ScoreStatus.Text = "Score: " + correctAnswers.ToString() + "/" + currQuestion.ToString();

            currQuestion++;
            QuestionsStatus.Text = "Question: " + currQuestion.ToString() + "/" + questionsNum;

            CurrQuestion.Text = question;
            Ans1.Content = ans1;
            Ans2.Content = ans2;
            Ans3.Content = ans3;
            Ans4.Content = ans4;
        }

        private void Ans1_Click(object sender, RoutedEventArgs e)
        {
            SendAnswer("1");
        }

        private void Ans2_Click(object sender, RoutedEventArgs e)
        {
            SendAnswer("2");
        }

        private void Ans3_Click(object sender, RoutedEventArgs e)
        {
            SendAnswer("3");
        }

        private void Ans4_Click(object sender, RoutedEventArgs e)
        {
            SendAnswer("4");
        }

        private void ExitGameButton_Click(object sender, RoutedEventArgs e)
        {
            // close window, return to menu window, send close room message
            MainWindow.TCPSendData("211");

            this.Owner.Show();
            this.Close();
        }
    }
}
