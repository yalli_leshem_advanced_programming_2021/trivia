﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for StatusWindow.xaml
    /// </summary>
    public partial class StatusWindow : Window
    {
        public StatusWindow()
        {
            InitializeComponent();
        }

        private void getPersonalStats()
        {
            // sends a request for personal stats, adds them to the texts
            MainWindow.TCPSendData("225");

            string rsp = MainWindow.TCPGetData();
   
            int currIndx = 3; // start from after the code (3 bytes)
            
            int gamesNo = int.Parse(rsp.Substring(currIndx, 4)); // len is 4 bytes
            currIndx += 4;

            int rightAnsNo = int.Parse(rsp.Substring(currIndx, 6)); // len is 6
            currIndx += 6;
            int wrongAnsNo = int.Parse(rsp.Substring(currIndx, 6)); // len is 6
            currIndx += 6;

            int avgTime1 = int.Parse(rsp.Substring(currIndx, 2)); // len is 2 bytes
            currIndx += 2;

            string avgTime2 = rsp.Substring(currIndx, 2); // len is 2 bytes

            NumOfGamesText.Text = "number of games - " + gamesNo.ToString();
            NumOfRightAnsText.Text = "number of right answers - " + rightAnsNo.ToString();
            NumOfWrongAnsText.Text = "number of wrong answers - " + wrongAnsNo.ToString();
            AvgTimeText.Text = "average time for answer - " + avgTime1.ToString() + "." + avgTime2;
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            NumOfGamesText.Text = "";
            NumOfRightAnsText.Text = "";
            NumOfWrongAnsText.Text = "";
            AvgTimeText.Text = "";
            getPersonalStats();
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            // close window and open menu window
            this.Owner.Show();
            this.Close();
        }
    }
}
