﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for JoinRoomWindow.xaml
    /// </summary>
    public partial class JoinRoomWindow : Window
    {
        public JoinRoomWindow(string LoggedUser)
        {
            InitializeComponent();
            ShowLoggedUser.Content = LoggedUser;
        }


        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            GetRoomsList();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            GetRoomsList();
        }

        private void GetRoomsList()
        {
            MainWindow.TCPSendData("205");
            string rsp = MainWindow.TCPGetData();
            Rooms.Items.Clear();

            int currIndx = 0;
            string msgCode = rsp.Substring(currIndx, 3); // len is 2 bytes
            currIndx += 3;

            if (msgCode == "106") // room list
            {
                int roomsNo = int.Parse(rsp.Substring(currIndx, 4));
                currIndx += 4;

                for (int i = 0; i < roomsNo; i++) // show all rooms
                {
                    string roomId = rsp.Substring(currIndx, 4);
                    currIndx += 4;

                    int roomLen = int.Parse(rsp.Substring(currIndx, 2)); // len is 2 bytes
                    currIndx += 2;
                    string room = rsp.Substring(currIndx, roomLen);
                    currIndx += roomLen;

                    Button b = new();
                    b.Name = "room" + roomId;
                    b.Tag = roomId;
                    b.Content = room;
                    b.BorderBrush = Rooms.Background;
                    b.Background = Rooms.Background;
                    Rooms.Items.Add(b);
                }
            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Owner.Show();
            this.Close();
        }

        private void JoinButton_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)Rooms.SelectedItem;
            if (b != null)
            {
                string joinRoomMsg = "209" + b.Tag;
                MainWindow.TCPSendData(joinRoomMsg);

                string rsp = MainWindow.TCPGetData();
                if (rsp[3] == '0')
                {
                    string time = rsp.Substring(6, 2);
                    string num = rsp.Substring(4, 2);
                    string waitingRoomInf = "Room: " + b.Content + "\nNumber Of Questions: " + num + "\nTime Per Round:" + time;
                    WaitingRoomWindow win = new(ShowLoggedUser.Content.ToString(), waitingRoomInf, time, num);
                    win.Owner = this.Owner;
                    win.Show();
                    this.Close();
                }
                else
                    GetRoomsList();
            }
        }
    }
}
