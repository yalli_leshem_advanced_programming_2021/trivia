﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Connect();
        }

        // declare member (field)
        static TcpClient tcpclnt;
        public static void Connect()
        {

            // use that field
            tcpclnt = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            tcpclnt.Connect(serverEndPoint);
        }

        public static void TCPSendData(string buffer)
        {
            NetworkStream netStream = tcpclnt.GetStream();
            byte[] sendbytes = Encoding.UTF8.GetBytes(buffer);
            netStream.Write(sendbytes, 0, sendbytes.Length);
        }

        public static string TCPGetData()
        {
            NetworkStream netStream = tcpclnt.GetStream();
            byte[] buffer = new byte[4096];
            int bufferLen = netStream.Read(buffer, 0, 4096);
            
            return Encoding.UTF8.GetString(buffer, 0, bufferLen);
        }

        bool alreadyReset = false;
        private void UsernameInput_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!alreadyReset) // only if it's the first click on the box
            {
                UsernameInput.Text = ""; // Clears the text field
                alreadyReset = true;
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            UsernameInput.Text = "Username";
            VisiblePass.Text = "Password";
            alreadyReset = false;
        }

        /// <summary>
        /// when hovering over the checkbox, show the hidden textbox 
        /// </summary>
        private void ShowPass_MouseEnter_1(object sender, MouseEventArgs e)
        {
            ShowPassText.Visibility = Visibility.Visible;

            // hide the passwordbox and show the textbox
            Password.Visibility = Visibility.Hidden;
            VisiblePass.Visibility = Visibility.Visible;
        }

        private void Password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            VisiblePass.Text = Password.Password;
        }

        private void ShowPass_MouseLeave_1(object sender, MouseEventArgs e)
        {
            ShowPassText.Visibility = Visibility.Hidden;
            VisiblePass.Visibility = Visibility.Hidden;
            Password.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// when mouse enters the password field, hide the text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VisiblePass_MouseEnter(object sender, MouseEventArgs e)
        {
            VisiblePass.Visibility = Visibility.Hidden;
        }

        private void Login_click(object sender, RoutedEventArgs e)
        {
            // when clicked, send data to the server
            // TODO:
            // send data to server 
            // login/throw error, depending on server response
            // if login was ok, close the login tab and go to the menu
            sendLoginMsg();
            
            int res = getLoginRsp();

            if (res == 0) // if 0 then succeeded
            {
                MenuWindow menuWin = new(UsernameInput.Text);
                menuWin.Owner = this;
                menuWin.Show();
                this.Hide();
            }
            else if (res == 1) // wrong details 
            {
                errorPopUp.Content = res.ToString() + "Wrong Details";
                UsernameInput.Text = "Username";
                VisiblePass.Text = "Password";
                Password.Password = "";
                alreadyReset = false;
            }
            else
            {
                errorPopUp.Content = res.ToString() + "User already connected";
                UsernameInput.Text = "Username";
                VisiblePass.Text = "Password";
                Password.Password = "";
                alreadyReset = false;
            }    
        }

        /// <summary>
        /// return the login message
        /// </summary>
        /// <returns></returns>
        private void sendLoginMsg()
        {
            string userLen = Convert.ToString(UsernameInput.Text.Length);
            string loginMsg = "200" + userLen.PadLeft(2, '0') + UsernameInput.Text;

            string passLen = Convert.ToString(Password.Password.Length);
            loginMsg += passLen.PadLeft(2, '0') + Password.Password;

            TCPSendData(loginMsg);
        }

        /// <summary>
        /// returns code
        /// </summary>
        /// <returns></returns>
        private int getLoginRsp()
        {
            string response = TCPGetData();
            return int.Parse(response[3].ToString());
        }

        private void Signup_click(object sender, RoutedEventArgs e)
        {
            Signup signUpWin = new();
            signUpWin.Owner = this;
            signUpWin.Show();
            this.Hide();
        }
    }
}
