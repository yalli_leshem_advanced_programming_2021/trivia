﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace triviaClient
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        public MenuWindow(string LoggedUser)
        {
            InitializeComponent();
            ShowLoggedUser.Content = LoggedUser;
        }

        private void QuitButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.TCPSendData("299");
            this.Owner.Close();
        }

        private void JoinRoomButton_Click(object sender, RoutedEventArgs e)
        {
            // make a new join room window
            // Hide main window
            JoinRoomWindow joinWin = new(ShowLoggedUser.Content.ToString());
            joinWin.Owner = this;
            this.Hide();
            joinWin.Show();
        }

        private void MyStatusButton_Click(object sender, RoutedEventArgs e)
        {
            StatusWindow statusWin = new(); // create new status window
            statusWin.Owner = this; // make the menu window it's owner
            this.Hide();
            statusWin.Show();
        }

        private void SignoutButton_Click(object sender, RoutedEventArgs e)
        {
            // send signout request to server, close this menu and open the login window
            MainWindow.TCPSendData("201");
            this.Owner.Show();
            this.Close();
        }

        private void BestScoresButton_Click(object sender, RoutedEventArgs e)
        {
            BestScoresWindow scoreWin = new(); // create new scores window
            scoreWin.Owner = this; // make the menu window it's owner
            this.Hide();
            scoreWin.Show();
        }

        private void CreateRoomButton_Click(object sender, RoutedEventArgs e)
        {
            CreateRoomWindow cRoomWin = new(ShowLoggedUser.Content.ToString()); // create new window
            cRoomWin.Owner = this; // make the menu window it's owner
            this.Hide();
            cRoomWin.Show();
        }
    }
}
